<?php

use App\Presentation\Web\Controller\Contract\ContactCreateController;
use App\Presentation\Web\Controller\Contract\ContactRemoveController;
use App\Presentation\Web\Controller\Contract\ContactShowListController;
use Symfony\Component\Routing\Loader\Configurator\RoutingConfigurator;

return static function (RoutingConfigurator $routes) {
    $routes->add('create_contact', '/create')
        ->controller(ContactCreateController::class)
        ->methods(['GET','POST']);

    $routes->add('show_contacts', '/')
        ->controller(ContactShowListController::class)
        ->methods(['GET']);

    $routes->add('remove_contact', '/{id}/remove')
        ->controller(ContactRemoveController::class)
        ->methods(['POST']);
};