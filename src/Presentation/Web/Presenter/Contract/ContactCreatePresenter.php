<?php

namespace App\Presentation\Web\Presenter\Contract;

use App\Core\Application\UseCase\Contact\Create\ContactCreateResponse;
use App\Core\Application\UseCase\Contact\Create\IContactCreatePresenter;
use App\Presentation\Web\Model\Contract\ContactCreateModel;

class ContactCreatePresenter implements IContactCreatePresenter
{
    public function present(ContactCreateResponse $createResponse): ContactCreateModel
    {
        $model = new ContactCreateModel();
        $model->name = $createResponse->firstName .' '.$createResponse->lastName;
        $model->address = $createResponse->address;
        $model->birthday = $createResponse->birthday;
        $model->city = $createResponse->city;
        $model->country = $createResponse->country;
        $model->email = $createResponse->email;
        $model->phoneNumber = $createResponse->phoneNumber;
        $model->picture = $createResponse->picture;
        $model->zip = $createResponse->zip;

        return $model;
    }
}