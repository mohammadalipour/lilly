<?php

namespace App\Presentation\Web\Presenter\Contract;

use App\Core\Application\UseCase\Contact\List\ContactListResponse;
use App\Core\Application\UseCase\Contact\List\IContactListPresenter;
use App\Presentation\Web\Model\Contract\ContactListModel;


class ContactListPresenter implements IContactListPresenter
{
    public function present(ContactListResponse $listResponse): ContactListModel
    {
        $model = new ContactListModel();
        $model->perPage = $listResponse->perPage;
        $model->page = $listResponse->page;
        $model->all = $listResponse->data;

        return $model;
    }
}