<?php

namespace App\Presentation\Web\Presenter\Contract;

use App\Core\Application\UseCase\Contact\Remove\ContactRemoveResponse;
use App\Core\Application\UseCase\Contact\Remove\IContactRemovePresenter;
use App\Presentation\Web\Model\Contract\ContactRemoveModel;


class ContactRemovePresenter implements IContactRemovePresenter
{
    public function present(ContactRemoveResponse $response): ContactRemoveModel
    {
        $model = new ContactRemoveModel();
        $model->id = $response->id;

        return $model;
    }
}