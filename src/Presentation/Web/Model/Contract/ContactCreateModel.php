<?php

namespace App\Presentation\Web\Model\Contract;

final class ContactCreateModel
{
    public string $name;
    public string $address;
    public \DateTime $birthday;
    public string $city;
    public string $country;
    public string $email;
    public string $phoneNumber;
    public string $picture;
    public int $zip;
}