<?php

namespace App\Presentation\Web\Model\Contract;

final class ContactListModel
{
    public array $all = [];
    public int $page = 1;
    public int $perPage = 10;
}