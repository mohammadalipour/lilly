<?php

namespace App\Presentation\Web\Controller\Contract;

use App\Core\Application\UseCase\Contact\List\ContactRemoveUseCase;
use App\Infrastructure\Framework\Form\FormRegistry;
use App\Infrastructure\Framework\Form\Type\Contact\ContactRemoveType;
use App\Presentation\Web\Presenter\Contract\ContactRemovePresenter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class ContactRemoveController extends AbstractController
{
    private FormRegistry $form;
    private ContactRemovePresenter $presenter;
    private ContactRemoveUseCase $useCase;

    public function __construct(
        FormRegistry           $form,
        ContactRemovePresenter $presenter,
        ContactRemoveUseCase   $useCase,
    )
    {
        $this->form = $form;
        $this->presenter = $presenter;
        $this->useCase = $useCase;
    }

    /**
     * @param int $id
     * @param Request $request
     * @return Response
     */
    public function __invoke(int $id, Request $request): Response
    {
        $form = $this->form->createForm(ContactRemoveType::class);
        $form->submit(['id' => $id, '_request' => $request->get('_method')]);

        $request = $form->getData();
        $response = $this->useCase->execute($request);
        $this->presenter->present($response);
        $this->addFlash('success', "Contact with id : {$id} Remove!");

        return $this->redirectToRoute('show_contacts');
    }
}