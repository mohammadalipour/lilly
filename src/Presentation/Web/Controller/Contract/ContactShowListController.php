<?php

namespace App\Presentation\Web\Controller\Contract;

use App\Core\Application\UseCase\Contact\List\ContactListUseCase;
use App\Infrastructure\Framework\Form\FormRegistry;
use App\Infrastructure\Framework\Form\Type\Contact\ContactListType;
use App\Presentation\Web\Presenter\Contract\ContactListPresenter;
use App\Presentation\Web\View\Contact\ContactListView;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

final class ContactShowListController extends AbstractController
{
    private FormRegistry $form;
    private ContactListPresenter $presenter;
    private ContactListUseCase $useCase;
    private ContactListView $view;

    public function __construct(
        FormRegistry         $form,
        ContactListPresenter $presenter,
        ContactListUseCase   $useCase,
        ContactListView      $view
    )
    {
        $this->form = $form;
        $this->presenter = $presenter;
        $this->useCase = $useCase;
        $this->view = $view;
    }

    /**
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws LoaderError
     */
    public function __invoke(Request $request): Response
    {
        $form = $this->form->createForm(ContactListType::class);
        $form->handleRequest($request);
        $form->submit(['page' => $request->get('page') ?? 1, 'perPage' => $request->get('perPage') ?? 10]);

        $request = $form->getData();
        $response = $this->useCase->query($request);
        $model = $this->presenter->present($response);

        return $this->view->generate($model);
    }
}