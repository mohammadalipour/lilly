<?php

namespace App\Presentation\Web\Controller\Contract;

use App\Core\Application\UseCase\Contact\Create\ContactCreateUseCase;
use App\Infrastructure\Framework\Form\FormRegistry;
use App\Infrastructure\Framework\Form\Type\Contact\ContactCreateType;
use App\Presentation\Web\Presenter\Contract\ContactCreatePresenter;
use App\Presentation\Web\View\Contact\ContactCreateView;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;

final class ContactCreateController extends AbstractController
{
    private FormRegistry $form;
    private ContactCreatePresenter $presenter;
    private ContactCreateUseCase $createUseCase;
    private ContactCreateView $view;

    public function __construct(
        FormRegistry           $form,
        ContactCreatePresenter $presenter,
        ContactCreateUseCase   $createUseCase,
        ContactCreateView      $view
    )
    {
        $this->form = $form;
        $this->presenter = $presenter;
        $this->createUseCase = $createUseCase;
        $this->view = $view;
    }

    /**
     * @throws RuntimeError
     * @throws LoaderError
     */
    public function __invoke(Request $request): Response
    {
        try {
            $form = $this->form->createForm(ContactCreateType::class);
            $form->handleRequest($request);

            if($form->isSubmitted()){
                $createRequest = $form->getData();
                $response = $this->createUseCase->execute($createRequest);
                $this->presenter->present($response);
                $this->addFlash('success', 'Contact Created!');

                return $this->redirect($request->getUri());
            }

            return $this->view->generate();
        } catch (\Exception $exception) {
            $this->addFlash('fail', $exception->getMessage());
            return $this->redirect($request->getUri());
        }
    }
}