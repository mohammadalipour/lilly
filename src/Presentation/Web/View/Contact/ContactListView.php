<?php

namespace App\Presentation\Web\View\Contact;

use App\Infrastructure\Framework\Form\FormRegistry;
use App\Infrastructure\Framework\Form\Type\Contact\ContactCreateType;
use App\Presentation\Web\Model\Contract\ContactListModel;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;

final class ContactListView
{
    private Environment $twig;
    private FormRegistry $formRegistry;

    public function __construct(Environment $twig, FormRegistry $formRegistry)
    {
        $this->twig = $twig;
        $this->formRegistry = $formRegistry;
    }

    /**
     * @param ContactListModel $model
     * @return Response
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function generate(ContactListModel $model): Response
    {
        return new Response($this->twig->render('Contact/contactList.html.twig', [
            'data' => $model,
        ]));
    }
}