<?php

namespace App\Presentation\Web\View\Contact;

use App\Infrastructure\Framework\Form\FormRegistry;
use App\Infrastructure\Framework\Form\Type\Contact\ContactCreateType;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;

final class ContactCreateView
{
    private Environment $twig;
    private FormRegistry $formRegistry;

    public function __construct(Environment $twig, FormRegistry $formRegistry)
    {
        $this->twig = $twig;
        $this->formRegistry = $formRegistry;
    }

    /**
     * @return Response
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function generate(): Response
    {
        $form = $this->formRegistry->getForm(ContactCreateType::class);

        return new Response($this->twig->render('Contact/createContact.html.twig', [
            'form' => $form->createView(),
        ]));
    }
}