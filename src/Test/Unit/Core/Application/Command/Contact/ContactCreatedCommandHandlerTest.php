<?php

namespace App\Test\Unit\Core\Application\Command\Contact;


use App\Core\Application\Command\Contact\ContactCreatedCommandHandler;
use App\Core\Application\Contract\IEventBus;
use App\Core\Domain\Command\Contact\ContactCreateCommand;
use App\Core\Domain\Model\Entity\Contact;
use App\Core\Domain\Repository\IContactRepository;
use DateTime;
use Exception;
use PHPUnit\Framework\TestCase;

class ContactCreatedCommandHandlerTest extends TestCase
{
    /**
     * @throws Exception
     */
    public function test_it_creates_contact_when_invoked()
    {
        $firstName = 'test_first_name';
        $lastName = 'test_last_name';
        $address = 'address';
        $birthday = DateTime::createFromFormat('Y-m-d', '1992/02/17');
        $city = 'tehran';
        $country = 'iran';
        $email = 'md.alipour91@gmail.com';
        $phoneNumber = 12345672;
        $picture = 'test.jpg';
        $zip = 123;

        $repository = $this->createMock(IContactRepository::class);
        $repository->expects(self::once())
            ->method('add')
            ->with(self::callback(
                fn(Contact $contact): bool => $contact->name() === $firstName . ' ' . $lastName
                    && $contact->address() === $address && $contact->birthday() === $birthday
                    && $contact->city() === $city && $contact->email() === $email
                    && $contact->country() === $country && $contact->phoneNumber() === $phoneNumber
                    && $contact->picture() === $picture && $contact->zip() === $zip));

        $eventBus = $this->createMock(IEventBus::class);
        $eventBus->expects(self::once())
            ->method('dispatchAll')
            ->with($repository);

        $command = new ContactCreateCommand(
            $firstName,
            $lastName,
            address: $address,
            email: $email,
            city: $city,
            birthday: $birthday,
            country: $country,
            phoneNumber: $phoneNumber,
            picture: $picture,
            zip: $zip
        );
        $handler = new ContactCreatedCommandHandler($eventBus, $repository);

        try {
            $handler($command);
        } catch (Exception $e) {
            if (!str_contains($e->getMessage(), 'id must not be accessed before initialization')) {
                throw $e;
            }
        }
    }
}