<?php

namespace App\Infrastructure\Framework\Form\Type\Contact;

use App\Core\Application\UseCase\Contact\Create\ContactCreateRequest;
use App\Core\Application\UseCase\Contact\List\ContactListRequest;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Positive;


final class ContactListType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('page', IntegerType::class, [
                'required' => false,
                'constraints' => [
                    new Positive(),
                    new Length(['max' => 3])
                ],
            ])->add('perPage', IntegerType::class, [
                'required' => false,
                'constraints' => [
                    new Positive(),
                    new Length(['max' => 3])
                ],

            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ContactListRequest::class,
            'csrf_protection' => false
        ]);
    }
}