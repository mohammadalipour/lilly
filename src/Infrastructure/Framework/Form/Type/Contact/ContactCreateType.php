<?php

namespace App\Infrastructure\Framework\Form\Type\Contact;

use App\Core\Application\UseCase\Contact\Create\ContactCreateRequest;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;


final class ContactCreateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                    new Length(['max' => 250])
                ],

            ])->add('lastName', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                    new Length(['max' => 250])
                ]
            ])->add('address', TextareaType::class, [
                'constraints' => [
                    new NotBlank(),
                ]
            ])->add('phoneNumber', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                    new Length(['max' => 11])
                ]
            ])->add('birthday', DateType::class, [
                'constraints' => [
                    new NotBlank(),
                ]
            ])->add('city', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                    new Length(['max' => 250])
                ]
            ])->add('country', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                    new Length(['max' => 250])
                ]
            ])->add('email', EmailType::class, [
                'constraints' => [
                    new NotBlank(),
                ]
            ])->add('phoneNumber', IntegerType::class, [
                'constraints' => [
                    new NotBlank()
                ]
            ])->add('zip', IntegerType::class, [
                'constraints' => [
                    new NotBlank(),
                ]
            ])->add('picture', FileType::class,['required' => false])
            ->add('submit', SubmitType::class,[
                'attr'=>[
                    'class'=>'btn btn-success pull-left'
                ]
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ContactCreateRequest::class,
            'csrf_protection' => false
        ]);
    }
}