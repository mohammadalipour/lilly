<?php

namespace App\Infrastructure\Notification;


use App\Core\Application\Event\Contact\IDoSomething;
use App\Core\Domain\Repository\IContactRepository;

final class DoSomething implements IDoSomething
{
    private IContactRepository $contactRepository;

    public function __construct(IContactRepository $contactRepository)
    {
        $this->contactRepository = $contactRepository;
    }

    public function execute($id): void
    {
        $this->contactRepository->get($id);

        return;
    }
}