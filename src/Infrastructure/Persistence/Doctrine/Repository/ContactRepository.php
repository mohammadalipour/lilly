<?php

namespace App\Infrastructure\Persistence\Doctrine\Repository;

use App\Core\Domain\Id;
use App\Core\Domain\Model\Entity\Contact;
use App\Core\Domain\Model\ValueObject\Contact\ContactId;
use App\Core\Domain\Repository\IContactRepository;
use App\Infrastructure\Persistence\Exception\ContactNotFoundException;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;

final class ContactRepository extends ServiceEntityRepository implements IContactRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Contact::class);
    }


    public function add(Contact $contact): void
    {
        $this->getEntityManager()->persist($contact);
    }

    /**
     * @param Id $contactId
     * @return Contact
     * @throws ContactNotFoundException
     */
    public function get(Id $contactId): Contact
    {
        $contact = $this->find($contactId);

        if (!$contact instanceof Contact) {
            throw ContactNotFoundException::byId($contactId);
        }

        return $contact;
    }

    public function all(int $page, int $perPage)
    {
        $dql = $this->createQueryBuilder('c')
            ->where('c.isDeleted=:isDeleted')
            ->setParameter('isDeleted',0)
            ->orderBy('c.createdAt', 'DESC')
            ->getQuery();
        $paginator = new Paginator($dql);

        return $paginator->getQuery()
            ->setFirstResult($perPage * ($page - 1))
            ->setMaxResults($perPage);
    }

    /**
     * @param ContactId $id
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(Id $id)
    {
        /** @var Contact $contact */
        $contact = $this->find($id->toString());
        $contact->softDelete();

        $this->getEntityManager()->persist($contact);
        $this->getEntityManager()->flush();
    }
}