<?php

namespace App\Core\Domain\Event\Contact;

use App\Core\Domain\Model\ValueObject\Contact\ContactId;

class ContactCreatedEvent
{
    private ContactId $contactId;

    public function __construct(ContactId $id)
    {
        $this->contactId = $id;
    }

    public function contactId(): ContactId
    {
        return $this->contactId;
    }
}