<?php

namespace App\Core\Domain\Command\Contact;

final class ContactRemoveCommand
{
    private int $id;


    public function __construct(int $id)
    {
        $this->id = $id;
    }

    public function id(): int
    {
        return $this->id;
    }
}