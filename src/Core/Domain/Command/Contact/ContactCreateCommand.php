<?php

namespace App\Core\Domain\Command\Contact;

use DateTime;

final class ContactCreateCommand
{
    private string $firstName;
    private string $lastName;
    private string $address;
    private string $city;
    private DateTime $birthday;
    private string $email;
    private string|null $picture;
    private string $phoneNumber;
    private int $zip;
    private string $country;


    public function __construct(
        string    $firstName,
        string    $lastName,
        string    $address,
        string    $email,
        string    $city,
        DateTime $birthday,
        string    $country,
        string    $phoneNumber,
        ?string    $picture,
        int       $zip
    )
    {
        $this->email = $email;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->address = $address;
        $this->city = $city;
        $this->country = $country;
        $this->birthday = $birthday;
        $this->phoneNumber = $phoneNumber;
        $this->picture = $picture;
        $this->zip = $zip;
    }

    /**
     * @return string
     */
    public function firstName(): string
    {
        return $this->firstName;
    }

    /**
     * @return string
     */
    public function lastName(): string
    {
        return $this->lastName;
    }

    /**
     * @return string
     */
    public function address(): string
    {
        return $this->address;
    }

    /**
     * @return string
     */
    public function city(): string
    {
        return $this->city;
    }

    /**
     * @return DateTime
     */
    public function birthday(): DateTime
    {
        return $this->birthday;
    }

    /**
     * @return string
     */
    public function email(): string
    {
        return $this->email;
    }

    /**
     * @return string|null
     */
    public function picture(): string|null
    {
        return $this->picture;
    }

    /**
     * @return string
     */
    public function phoneNumber(): string
    {
        return $this->phoneNumber;
    }

    /**
     * @return int
     */
    public function zip(): int
    {
        return $this->zip;
    }

    /**
     * @return string
     */
    public function country(): string
    {
        return $this->country;
    }
}