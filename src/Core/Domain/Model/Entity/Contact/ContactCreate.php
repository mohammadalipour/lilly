<?php

namespace App\Core\Domain\Model\Entity\Contact;

use App\Core\Domain\Command\Contact\ContactCreateCommand;
use App\Core\Domain\Model\Entity\Contact;
use App\Core\Domain\Model\ValueObject\Contact\Address;
use App\Core\Domain\Model\ValueObject\Contact\Birthday;
use App\Core\Domain\Model\ValueObject\Contact\City;
use App\Core\Domain\Model\ValueObject\Contact\Country;
use App\Core\Domain\Model\ValueObject\Contact\Email;
use App\Core\Domain\Model\ValueObject\Contact\Name;
use App\Core\Domain\Model\ValueObject\Contact\PhoneNumber;
use App\Core\Domain\Model\ValueObject\Contact\Picture;
use App\Core\Domain\Model\ValueObject\Contact\Zip;
use Exception;

class ContactCreate
{
    /**
     * @throws Exception
     */
    public static function execute(ContactCreateCommand $command): Contact
    {
        return new Contact(
            name: new Name($command->firstName(),$command->lastName()),
            address: new Address($command->address()),
            email: new Email($command->email()),
            birthday: new Birthday($command->birthday()),
            city: new City($command->city()),
            country: new Country($command->country()),
            phoneNumber: new PhoneNumber($command->phoneNumber()),
            picture: new Picture($command->picture()),
            zip: new Zip($command->zip()),
        );
    }
}