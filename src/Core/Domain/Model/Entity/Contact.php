<?php

namespace App\Core\Domain\Model\Entity;

use App\Core\Domain\Command\Contact\ContactCreateCommand;
use App\Core\Domain\Event\Contact\ContactCreatedEvent;
use App\Core\Domain\Model\Entity\Contact\ContactCreate;
use App\Core\Domain\Model\ValueObject\Contact\Address;
use App\Core\Domain\Model\ValueObject\Contact\Birthday;
use App\Core\Domain\Model\ValueObject\Contact\City;
use App\Core\Domain\Model\ValueObject\Contact\ContactId;
use App\Core\Domain\Model\ValueObject\Contact\Country;
use App\Core\Domain\Model\ValueObject\Contact\Email;
use App\Core\Domain\Model\ValueObject\Contact\Name;
use App\Core\Domain\Model\ValueObject\Contact\PhoneNumber;
use App\Core\Domain\Model\ValueObject\Contact\Picture;
use App\Core\Domain\Model\ValueObject\Contact\Zip;
use App\Core\Domain\RaiseEvents;
use App\Core\Domain\Time;
use DateTimeInterface;
use Exception;

class Contact
{
    use RaiseEvents;

    private $id;
    private Name $name;
    private Birthday $birthday;
    private Address $address;
    private Email $email;
    private City $city;
    private Country $country;
    private PhoneNumber $phoneNumber;
    private Picture $picture;
    private Zip $zip;
    private DateTimeInterface $createdAt;
    private bool $isDeleted = false;

    /**
     * @throws Exception
     */
    public function __construct(
        Name        $name,
        Address     $address,
        Email       $email,
        Birthday    $birthday,
        City        $city,
        Country     $country,
        PhoneNumber $phoneNumber,
        Picture     $picture,
        Zip         $zip,
        bool        $isDeleted
    )
    {
        $this->id = ContactId::generate();
        $this->email = $email;
        $this->name = $name;
        $this->address = $address;
        $this->birthday = $birthday;
        $this->city = $city;
        $this->country = $country;
        $this->phoneNumber = $phoneNumber;
        $this->picture = $picture;
        $this->zip = $zip;
        $this->createdAt = Time::now();
        $this->isDeleted = false;

        $this->raise(new ContactCreatedEvent($this->id));
    }

    /**
     * @throws Exception
     */
    public static function create(ContactCreateCommand $create): self
    {
        return ContactCreate::execute($create);
    }

    /**
     * @return ContactId
     */
    public function id()
    {
        return $this->id;
    }

    /**
     * @return Name
     */
    public function name(): Name
    {
        return $this->name;
    }

    /**
     * @return Address
     */
    public function address(): Address
    {
        return $this->address;
    }

    /**
     * @return Email
     */
    public function email(): Email
    {
        return $this->email;
    }

    /**
     * @return City
     */
    public function city(): City
    {
        return $this->city;
    }

    /**
     * @return Country
     */
    public function country(): Country
    {
        return $this->country;
    }

    /**
     * @return PhoneNumber
     */
    public function phoneNumber(): PhoneNumber
    {
        return $this->phoneNumber;
    }

    /**
     * @return Picture
     */
    public function picture(): Picture
    {
        return $this->picture;
    }

    /**
     * @return Zip
     */
    public function zip(): Zip
    {
        return $this->zip;
    }

    /**
     * @return DateTimeInterface
     */
    public function createdAt(): DateTimeInterface
    {
        return $this->createdAt;
    }

    public function birthday(): Birthday
    {
        return $this->birthday;
    }

    public function isDeleted(): bool
    {
        return $this->isDeleted;
    }

    /**
     * @return $this
     */
    public function softDelete(): static
    {
        $this->isDeleted = true;

        return $this;
    }

}