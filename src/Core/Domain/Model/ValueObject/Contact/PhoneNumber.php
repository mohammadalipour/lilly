<?php

namespace App\Core\Domain\Model\ValueObject\Contact;


use App\Core\Domain\Validation\IsBlank\IsBlank;
use App\Core\Domain\Validation\IsNumeric\IsNumeric;
use App\Core\Domain\Validation\MaxLength\MaxLength;

final class PhoneNumber
{
    private string $phoneNumber;

    public function __construct(string $phoneNumber)
    {
        IsBlank::execute($phoneNumber);
        IsNumeric::execute($phoneNumber);
        (new MaxLength())->setMaxLength(11)::execute($phoneNumber);

        $this->phoneNumber = $phoneNumber;
    }

    public function __toString(): string
    {
        return $this->toString();
    }

    public function toString(): string
    {
        return $this->phoneNumber;
    }
}
