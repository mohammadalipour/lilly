<?php

namespace App\Core\Domain\Model\ValueObject\Contact;


use App\Core\Domain\Validation\IsBlank\IsBlank;
use App\Core\Domain\Validation\MaxLength\MaxLength;

final class Name
{
    private string $firstname;
    private string $lastname;

    public function __construct(string $firstname, string $lastname)
    {
        IsBlank::execute($firstname);
        IsBlank::execute($lastname);
        (new MaxLength())->setMaxLength(255)::execute($firstname);
        (new MaxLength())->setMaxLength(255)::execute($lastname);


        $this->firstname = $firstname;
        $this->lastname = $lastname;
    }

    public function firstname(): string
    {
        return $this->firstname;
    }

    public function lastname(): string
    {
        return $this->lastname;
    }

    public function toString(): string
    {
        return $this->firstname. ' '. $this->lastname;
    }

    public function __toString(): string
    {
        return $this->toString();
    }
}
