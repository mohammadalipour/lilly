<?php

namespace App\Core\Domain\Model\ValueObject\Contact;

use App\Core\Domain\Validation\MaxLength\MaxLength;

final class Picture
{
    private string $picture;

    public function __construct(string $picture)
    {
        (new MaxLength())->setMaxLength(255)::execute($picture);

        $this->picture = $picture;
    }

    public function picture(): string
    {
        return $this->picture;
    }

    public function __toString(): string
    {
        return $this->toString();
    }

    public function toString(): string
    {
        return $this->picture;
    }
}
