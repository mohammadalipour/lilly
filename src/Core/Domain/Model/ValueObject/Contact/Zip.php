<?php

namespace App\Core\Domain\Model\ValueObject\Contact;


use App\Core\Domain\Validation\IsBlank\IsBlank;
use App\Core\Domain\Validation\IsNumeric\IsNumeric;
use App\Core\Domain\Validation\MaxLength\MaxLength;

final class Zip
{
    private int $zip;

    public function __construct(int $zip)
    {
        IsBlank::execute($zip);
        IsNumeric::execute($zip);
        (new MaxLength())->setMaxLength(5)::execute($zip);

        $this->zip = $zip;
    }

    public function __toString(): string
    {
        return $this->toString();
    }

    public function toString(): string
    {
        return $this->zip;
    }
}
