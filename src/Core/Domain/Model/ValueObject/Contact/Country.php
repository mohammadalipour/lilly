<?php

namespace App\Core\Domain\Model\ValueObject\Contact;

use App\Core\Domain\Validation\IsBlank\IsBlank;
use App\Core\Domain\Validation\MaxLength\MaxLength;

final class Country
{
    private string $country;

    public function __construct(string $country)
    {
        IsBlank::execute($country);
        (new MaxLength())->setMaxLength(255)::execute($country);

        $this->country = $country;
    }

    public function country(): string
    {
        return $this->country;
    }


    public function toString(): string
    {
        return $this->country;
    }

    public function __toString(): string
    {
        return $this->toString();
    }
}
