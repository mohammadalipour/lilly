<?php

namespace App\Core\Domain\Model\ValueObject\Contact;

use App\Core\Domain\Validation\IsBlank\IsBlank;

final class Address
{
    private string $address;

    public function __construct(string $address)
    {
        IsBlank::execute($address);

        $this->address = $address;
    }

    public function address(): string
    {
        return $this->address;
    }

    public function __toString(): string
    {
        return $this->toString();
    }

    public function toString(): string
    {
        return $this->address;
    }
}
