<?php

namespace App\Core\Domain\Model\ValueObject\Contact;


use App\Core\Domain\Validation\IsBlank\IsBlank;
use App\Core\Domain\Validation\IsEmail\IsEmail;
use App\Core\Domain\Validation\MaxLength\MaxLength;

final class Email
{
    private string $email;

    public function __construct(string $email)
    {
        IsBlank::execute($email);
        IsEmail::execute($email);
        (new MaxLength())->setMaxLength(255)::execute($email);

        $this->email = $email;
    }

    public function toString(): string
    {
        return $this->email;
    }

    public function __toString(): string
    {
        return $this->toString();
    }
}
