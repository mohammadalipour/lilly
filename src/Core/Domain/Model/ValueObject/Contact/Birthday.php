<?php

namespace App\Core\Domain\Model\ValueObject\Contact;


use App\Core\Domain\Validation\IsBlank\IsBlank;
use App\Core\Domain\Validation\IsDate\IsDate;
use App\Core\Domain\Validation\MaxLength\MaxLength;

final class Birthday
{
    private \DateTime $birthday;

    public function __construct(\DateTime $birthday)
    {
        IsBlank::execute($birthday);

        $this->birthday = $birthday;
    }

    public function toString()
    {
        return $this->birthday->format('Y-m-d');
    }


    public function __toString(): string
    {
        return $this->toString();
    }
}
