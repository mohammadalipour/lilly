<?php

namespace App\Core\Domain\Model\ValueObject\Contact;

use App\Core\Domain\Validation\IsBlank\IsBlank;
use App\Core\Domain\Validation\MaxLength\MaxLength;

final class City
{
    private string $city;

    public function __construct(string $city)
    {
        IsBlank::execute($city);
        (new MaxLength())->setMaxLength(255)::execute($city);

        $this->city = $city;
    }

    public function city(): string
    {
        return $this->city;
    }


    public function toString(): string
    {
        return $this->city;
    }

    public function __toString(): string
    {
        return $this->toString();
    }
}
