<?php

namespace App\Core\Domain\Repository;

use App\Core\Domain\Id;
use App\Core\Domain\Model\Entity\Contact;

interface IContactRepository
{
    public function add(Contact $contact): void;

    public function get(Id $contactId): Contact;

    public function all(int $page, int $perPage);

    public function remove(Id $id);
}