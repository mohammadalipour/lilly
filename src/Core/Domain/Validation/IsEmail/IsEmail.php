<?php

namespace App\Core\Domain\Validation\IsEmail;

use App\Core\Domain\Validation\IValidator;

class IsEmail implements IValidator
{

    public static function execute($value): bool
    {
        if(!filter_var($value, FILTER_VALIDATE_EMAIL)) {
            throw new \RuntimeException("The value dose not email");
        }

        return true;
    }
}