<?php

namespace App\Core\Domain\Validation\IsBoolean;

use App\Core\Domain\Validation\IValidator;

class IsBoolean implements IValidator
{
    public static function execute($value): bool
    {
        if (is_bool($value)){
            throw new \RuntimeException("The value dose not kind of boolean value");
        }

        return true;
    }
}