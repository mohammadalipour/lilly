<?php

namespace App\Core\Domain\Validation\IsDate;

use App\Core\Domain\Validation\IValidator;

class IsDate implements IValidator
{

    public static function execute($value): bool
    {
        $date = \DateTime::createFromFormat('Y-m-d', $value);

        if(!$date && $date->format('Y-m-d') !== $value){
            throw new \RuntimeException("The date format doses not valid");
        }

        return true;
    }
}