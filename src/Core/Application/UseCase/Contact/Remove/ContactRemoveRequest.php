<?php

namespace App\Core\Application\UseCase\Contact\Remove;

class ContactRemoveRequest
{
    public int $id;
}