<?php

namespace App\Core\Application\UseCase\Contact\Remove;

final class ContactRemoveResponse
{
    public int $id;
}