<?php

namespace App\Core\Application\UseCase\Contact\Remove;

interface IContactRemovePresenter
{
    public function present(ContactRemoveResponse $response);
}