<?php

namespace App\Core\Application\UseCase\Contact\List;

class ContactListRequest
{
    public int $page = 1;
    public int $perPage = 10;
}