<?php

namespace App\Core\Application\UseCase\Contact\List;

use App\Core\Application\Contract\ICommandBus;
use App\Core\Application\UseCase\Contact\Remove\ContactRemoveRequest;
use App\Core\Application\UseCase\Contact\Remove\ContactRemoveResponse;
use App\Core\Domain\Command\Contact\ContactRemoveCommand;

class ContactRemoveUseCase
{
    private ICommandBus $commandBus;

    public function __construct(ICommandBus $queryBus)
    {
        $this->commandBus = $queryBus;
    }

    /**
     * @param ContactRemoveRequest $request
     * @return ContactRemoveResponse
     */
    public function execute(ContactRemoveRequest $request): ContactRemoveResponse
    {
        $response = new ContactRemoveResponse();

        $this->commandBus->execute(new ContactRemoveCommand(
            $request->id,
        ));

        $response->id = $request->id;

        return $response;
    }
}