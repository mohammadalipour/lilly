<?php

namespace App\Core\Application\UseCase\Contact\List;

interface IContactListPresenter
{
    public function present(ContactListResponse $listResponse);
}