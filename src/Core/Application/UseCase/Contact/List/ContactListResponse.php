<?php

namespace App\Core\Application\UseCase\Contact\List;

final class ContactListResponse
{
    public array $data;
    public int $page;
    public int $perPage;
}