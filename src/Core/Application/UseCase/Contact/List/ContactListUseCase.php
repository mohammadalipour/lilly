<?php

namespace App\Core\Application\UseCase\Contact\List;

use App\Core\Application\Contract\ICommandBus;
use App\Core\Application\Contract\IQueryBus;
use App\Core\Domain\Command\Contact\ContactCreateCommand;
use App\Core\Domain\Query\Contact\ContactListQuery;

class ContactListUseCase
{
    private IQueryBus $queryBus;

    public function __construct(IQueryBus $queryBus)
    {
        $this->queryBus = $queryBus;
    }

    public function query(ContactListRequest $request):ContactListResponse
    {
            $response = new ContactListResponse();

            $result = $this->queryBus->query(new ContactListQuery(
                $request->page,
                $request->perPage
            ));


            $response->perPage = $request->perPage;
            $response->page = $request->page;
            $response->data = $result->getResult();

            return $response;
    }
}