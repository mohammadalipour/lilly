<?php

namespace App\Core\Application\UseCase\Contact\Create;

class ContactCreateRequest
{
    public string $firstName;
    public string $lastName;
    public string $address;
    public \DateTime $birthday;
    public string $city;
    public string $country;
    public string $email;
    public string $phoneNumber;
    public null|string $picture;
    public int $zip;
}