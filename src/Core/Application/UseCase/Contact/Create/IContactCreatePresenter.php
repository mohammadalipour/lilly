<?php

namespace App\Core\Application\UseCase\Contact\Create;

interface IContactCreatePresenter
{
    public function present(ContactCreateResponse $createResponse);
}