<?php

namespace App\Core\Application\UseCase\Contact\Create;

use App\Core\Application\Contract\ICommandBus;
use App\Core\Domain\Command\Contact\ContactCreateCommand;

class ContactCreateUseCase
{
    private ICommandBus $commandBus;

    public function __construct(ICommandBus $commandBus)
    {
        $this->commandBus = $commandBus;
    }

    public function execute(ContactCreateRequest $request):ContactCreateResponse
    {
            $response = new ContactCreateResponse();

            $this->commandBus->execute(new ContactCreateCommand(
                $request->firstName,
                $request->lastName,
                $request->address,
                $request->email,
                $request->city,
                $request->birthday,
                $request->country,
                $request->phoneNumber,
                $request->picture ?? '',
                $request->zip
            ));

            $response->firstName = $request->firstName;
            $response->lastName = $request->lastName;
            $response->address = $request->address;
            $response->email = $request->email;
            $response->city = $request->city;
            $response->birthday = $request->birthday;
            $response->phoneNumber = $request->phoneNumber;
            $response->country = $request->country;
            $response->picture = $request->picture ?? '';
            $response->zip = $request->zip;

            return $response;
    }
}