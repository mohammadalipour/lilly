<?php

namespace App\Core\Application\Event\Contact;

interface IDoSomething
{
    public function execute($id);
}