<?php

namespace App\Core\Application\Event\Contact;


use App\Core\Domain\Event\Contact\ContactCreatedEvent;

final class ContactCreatedHandler
{
    private IDoSomething $doSomething;

    public function __construct(IDoSomething $doSomething)
    {
        $this->doSomething = $doSomething;
    }
    /**
     * @param ContactCreatedEvent $created
     */
    public function __invoke(ContactCreatedEvent $created)
    {
        return true;
    }
}