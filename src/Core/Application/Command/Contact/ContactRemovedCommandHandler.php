<?php

namespace App\Core\Application\Command\Contact;

use App\Core\Application\Contract\IEventBus;
use App\Core\Domain\Command\Contact\ContactRemoveCommand;
use App\Core\Domain\Id;
use App\Core\Domain\Model\ValueObject\Contact\ContactId;
use App\Core\Domain\Repository\IContactRepository;

final class ContactRemovedCommandHandler
{
    private IEventBus $busEvent;
    private IContactRepository $contactRepository;

    public function __construct(IEventBus $busEvent, IContactRepository $contactRepository)
    {
        $this->busEvent = $busEvent;
        $this->contactRepository = $contactRepository;
    }

    /**
     * @param ContactRemoveCommand $command
     */
    public function __invoke(ContactRemoveCommand $command): void
    {
        $this->contactRepository->remove(ContactId::fromString($command->id()));

        $this->busEvent->dispatchAll([]);
    }
}