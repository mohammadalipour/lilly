<?php

namespace App\Core\Application\Command\Contact;

use App\Core\Application\Contract\IEventBus;
use App\Core\Domain\Command\Contact\ContactCreateCommand;
use App\Core\Domain\Model\Entity\Contact;
use App\Core\Domain\Repository\IContactRepository;
use Exception;

final class ContactCreatedCommandHandler
{
    private IEventBus $busEvent;
    private IContactRepository $contactRepository;

    public function __construct(IEventBus $busEvent, IContactRepository $contactRepository)
    {
        $this->busEvent = $busEvent;
        $this->contactRepository = $contactRepository;
    }

    /**
     * @param ContactCreateCommand $create
     * @throws Exception
     */
    public function __invoke(ContactCreateCommand $create): void
    {
        $contact = Contact::create($create);
        $this->contactRepository->add($contact);

        $this->busEvent->dispatchAll($contact->pop());
    }
}