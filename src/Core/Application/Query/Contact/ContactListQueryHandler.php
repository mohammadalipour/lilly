<?php

namespace App\Core\Application\Query\Contact;

use App\Core\Application\Contract\IQueryBus;
use App\Core\Domain\Query\Contact\ContactListQuery;
use App\Core\Domain\Repository\IContactRepository;
use Exception;

final class ContactListQueryHandler
{
    private IQueryBus $queryBus;
    private IContactRepository $contactRepository;

    public function __construct(IQueryBus $queryBus, IContactRepository $contactRepository)
    {
        $this->queryBus = $queryBus;
        $this->contactRepository = $contactRepository;
    }

    /**
     * @param ContactListQuery $query
     * @throws Exception
     */
    public function __invoke(ContactListQuery $query)
    {
        return $this->contactRepository->all($query->getPage(),$query->getPerPage());
    }
}